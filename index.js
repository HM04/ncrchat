const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const uuidv4 = require("uuid/v4");
const Chatkit = require("@pusher/chatkit-server");

const app = express();
const chatkit = new Chatkit.default(require("./config.js"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "assets")));

app.post("/session/auth", (req, res) => {
  const authData = chatkit.authenticate({ userId: req.query.user_id });
  res.status(authData.status).send(authData.body);
});

app.post("/session/load", (req, res, next) => {
  let createdUser = null;

  console.log(req.body);
  chatkit
    .createUser({
      id: req.body.email,
      name: req.body.name,
      subject: req.body.subject
    })
    .then(user => {
      createdUser = user;
      getUserRoom(req, res, next, false);
    })
    .catch(err => {
      if (err.error === "services/chatkit/user_already_exists") {
        createdUser = {
          id: req.body.email
        };

        getUserRoom(req, res, next, true);
        return;
      }

      next(err);
    });

  function getUserRoom(req, res, next, existingAccount) {
    const name = createdUser.name;
    const email = createdUser.id;
    const subject = createdUser.subject;


    chatkit
      .getUserRooms({
        userID: createdUser.id
      })
      .then(rooms => {
        let clientRoom = null;
        clientRoom = rooms.filter(room => {
          return room.name === createdUser.id;
        });

        if (clientRoom && clientRoom.id) {
          return res.json(clientRoom);
        }

        chatkit
          .createRoom({
            creatorId: createdUser.id,
            isPrivate: true,
            name: createdUser.id,
            userIds: ["chatkit-dashboard", createdUser.id],
          })
          .then(room => res.json(room))
          .catch(err => {
            console.log(err);
            next(new Error(`${err.error_type} - ${err.error_description}`));
          });
      })
      .catch(err => {
        console.log(err);
        next(new Error(`ERROR: ${err.error_type} - ${err.error_description}`));
      });
  }
});

app.get("/", (req, res) => {
  res.sendFile("index.html", { root: __dirname + "/views" });
});

app.get("/admin", (req, res) => {
  res.sendFile("admin.html", { root: __dirname + "/views" });
});

app.listen(3000, () => console.log("Application listening on port 3000!!!"));
